/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.AccountDB;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

class RecordNotFoundException extends SQLException {

    public RecordNotFoundException() {
    }

    public RecordNotFoundException(String msg) {
        super(msg);
    }

    public RecordNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
/**
 *
 * @author 1796199
 */
public class Database {
    
    /*MySQL connection series at school
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "tansactions";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";
    */
    
    /*MySQL connection series at home*/
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "tansactions";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "33110532ninipig";
    
    private Connection conn;
    public void commit() throws SQLException{
        this.conn.commit();
    }
    public void rollback() throws SQLException{
        this.conn.rollback();
    }
    public void setAutoCommit(boolean ifCommit) throws SQLException{
        this.conn.setAutoCommit(ifCommit);
    }
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);        
    }
    
    public void addTransaction(Transaction trans) throws SQLException {
        String sql = "INSERT INTO transactions (deposit, withdrawl) VALUES (?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, trans.getDeposit());
            stmt.setBigDecimal(2, trans.getWithdrawl());
            stmt.executeUpdate();
        }
    }   
    
    public void addAvailableTransaction(Transaction trans) throws SQLException {
        String sql = "INSERT INTO transactions (deposit, withdrawl,opDate) VALUES (?, ?,?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, trans.getDeposit());
            stmt.setBigDecimal(2, trans.getWithdrawl());
            stmt.setDate(3, trans.getTimeStamp());
            stmt.executeUpdate();
        }
    }   
    
    /*
    public Todo getTodoById(int id) throws SQLException {
        // FIXME: Preapred statement is required if id may contain malicious SQL injection code
        String sql = "SELECT * FROM todos WHERE id=" + id;
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                String task = result.getString("task");
                java.sql.Date dueDateSql = result.getDate("dueDate");
                String isDoneStr = result.getString("isDone");
                Todo todo = new Todo(id, task, dueDateSql, isDoneStr);
                return todo;
            } else {
                throw new SQLException("Record not found");
                //return null;
            }
        }
    }
    
    public void updateTodo(Todo todo) throws SQLException {
        String sql = "UPDATE todos SET task=?, dueDate=?, isDone=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, todo.getTask());
            stmt.setDate(2, todo.getDueDateSql());
            stmt.setString(3, todo.isDoneString());
            // where is the last parameter
            stmt.setInt(4, todo.getId());
            
            stmt.executeUpdate();
        }
    }
    
    public void deleteTodoById(int id) throws SQLException {
        String sql = "DELETE FROM todos WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);            
            stmt.executeUpdate();
        }
    }
*/
    public ArrayList<Transaction> getAllTransactions() throws SQLException {
        String sql = "SELECT * FROM transactions order by id";
        ArrayList<Transaction> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                long id = result.getInt("id");
                BigDecimal tDeposit = result.getBigDecimal("deposit");
                BigDecimal tWithDrawl = result.getBigDecimal("withdrawl");                
                Timestamp tTimestamp = result.getTimestamp("opdate");
                Date tDate = new Date(tTimestamp.getTime()); 
                Transaction tTrans = new Transaction(id, tDeposit, tWithDrawl, tDate);
                list.add(tTrans);
            }
        }
        return list;
    }
    
    public BigDecimal getBalance() throws SQLException{
        String sql = "select sum(deposit)-sum(withdrawl) balance from transactions";
        BigDecimal tbalance =  BigDecimal.ZERO;
        try (Statement stmt = conn.createStatement()) {
            
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                tbalance  = result.getBigDecimal("balance");  
            }                      
        } 
        return tbalance;
    }   
}
