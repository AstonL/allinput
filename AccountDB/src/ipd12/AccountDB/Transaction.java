/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.AccountDB;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author 1796199
 */
public class Transaction {
    private long id;    
    private BigDecimal deposit;
    private BigDecimal withdrawl;
    private Date timeStamp;

    public static final SimpleDateFormat sdf = new SimpleDateFormat("MMM-d-yyyy");
    
    public Transaction(long id, BigDecimal deposit, BigDecimal withdrawl, Date timeStamp) {
        setId(id);
        setDeposit(deposit);
        setWithdrawl(withdrawl);
        setTimeStamp(timeStamp);
    }

    public Transaction(BigDecimal deposit, BigDecimal withdrawl, Date timeStamp) {
        setDeposit(deposit);
        setWithdrawl(withdrawl);
        setTimeStamp(timeStamp);
    }

    @Override
    public String toString() {
        String tdate = sdf.format(this.timeStamp);
        return "Transaction{" + "id=" + id + ", deposit=" + deposit + ", withdrawl=" + withdrawl + ", timeStamp=" + tdate + '}';
    }

    public Transaction(BigDecimal deposit, BigDecimal withdrawl) {
        this.id = 1;
        setDeposit(deposit);
        setWithdrawl(withdrawl);
        this.timeStamp = new Date(4773);
    }

   

  
    
    /**
     * @return the id
     */
    public long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(long id) {
        this.id = id;
    }

    /**
     * @return the deposit
     */
    public BigDecimal getDeposit() {
        return this.deposit;
    }

    /**
     * @param deposit the deposit to set
     */
    public final void setDeposit(BigDecimal deposit) {
        if(deposit.compareTo(new BigDecimal(0))<0)
        {
            JOptionPane.showMessageDialog(null,
                        "input invalid error!\n",
                        "input invalid error!",
                        JOptionPane.ERROR_MESSAGE);
            throw new IllegalArgumentException("Inputed number of deposit is invalid");
        }
             
        this.deposit = deposit;
    }

    
    /**
     * @return the withdrawl
     */
    public BigDecimal getWithdrawl() {
        return withdrawl;
    }

    /**
     * @param withdrawl the withdrawl to set
     */
    public final void setWithdrawl(BigDecimal withdrawl) {
        
        if(withdrawl.compareTo(new BigDecimal(0))<0)
        {
            JOptionPane.showMessageDialog(null,
                        "input invalid error!\n",
                        "input invalid error!",
                        JOptionPane.ERROR_MESSAGE);
            throw new IllegalArgumentException("Inputed number of withdrawl is invalid");
        }
        this.withdrawl = withdrawl;
    }

    /**
     * @return the timeStamp
     */
    public Date getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the timeStamp to set
     */
    public final void setTimeStamp(Date timeStamp) {        
        this.timeStamp=timeStamp;
    }
                                    
}
