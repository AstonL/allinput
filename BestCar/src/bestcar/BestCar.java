/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import jdk.nashorn.internal.parser.TokenType;

/**
 *
 * @author 莫亚泥
 */
class CarComparatorByMaxSpeed implements Comparator<Car>{

    @Override
    public int compare(Car o1, Car o2) {
        return o1.maxSpeedKmph-o2.maxSpeedKmph; //To change body of generated methods, choose Tools | Templates.
    }    
}
class CarComparatorByAccelleration implements Comparator<Car>{

    @Override
    public int compare(Car o1, Car o2) {
        double c = o1.secTo100Kmph-o2.secTo100Kmph;
        if(c<0){
            return -1;
        }else if(c>0){
            return 1;
        }else {
            return 0;
        }        
    }    
}
class CarComparatorByEconomy implements Comparator<Car>{

    @Override
    public int compare(Car o1, Car o2) {
        double c = o1.litersPer100km-o2.litersPer100km;
        if(c<0){
            return -1;
        }else if(c>0){
            return 1;
        }else {
            return 0;
        }    //To change body of generated methods, choose Tools | Templates.
    }    
}
public class BestCar {

    /**
     * @param args the command line arguments
     */
    static ArrayList<Car> aList = new ArrayList<>();
    public static void main(String[] args) {
        // TODO code application logic here
        
        aList.add(new Car("Toyota", "corola", 220, 1.42342, 5.6));
        aList.add(new Car("Ferrari", "911", 400, 1.42341, 15));
        aList.add(new Car("Mazada", "Mazada 3", 500, 1.42345, 5.4));
        aList.add(new Car("Subaru", "forest", 300, 1.42348, 8.3));
        aList.add(new Car("Jeep", "explorer", 260, 1.42343, 9.6));
        aList.add(new Car("Mecerdes", "B210", 280, 1.4234156, 7.7));
        aList.add(new Car("Toyota", "Rav", 280, 1.4234155, 6.6));
        aList.add(new Car("Mazada", "Mazada 6", 300, 1.42341567, 8.6));
        aList.add(new Car("Hymmha", "sdafsf", 280, 1.4234, 6.6));
        aList.add(new Car("Mini", "Mazaaggda 6", 300, 1.423434, 8.6));
        
        Collections.sort(aList);
        for(Car c : aList){            
            System.out.println(c);
        }
        System.out.println("==================Sort by MaxSpeed=================");
        Collections.sort(aList,Car.MaxSpeedComparator);
        for(Car c : aList){            
            System.out.println(c);
        }
        System.out.println("==================Sort by Acceleration=================");
        Collections.sort(aList,bestcar.Car.AccellerationComparator);
        for(bestcar.Car c : aList){    
            System.out.println(c);
        }
         System.out.println("==================Sort by Economy=================");
        Collections.sort(aList,bestcar.Car.EconomyComparator);
        for(bestcar.Car c : aList){    
            System.out.println(c);
        }    
           
    }
    
}

class Car implements  Comparable<Car>{
	String make; // e.g. BMW, Toyota, etc.
	String model; // e.g. X5, Corolla, etc.
	int maxSpeedKmph; // maximum speed in km/h
	double secTo100Kmph; // accelleration time 1-100 km/h in seconds
	double litersPer100km; // economy: liters of gas per 100 km
        static Comparator<Car> MaxSpeedComparator = new CarComparatorByMaxSpeed();
        static Comparator<Car> AccellerationComparator = new CarComparatorByAccelleration();
        static Comparator<Car> EconomyComparator= new CarComparatorByEconomy();
                
    @Override
    public String toString() {
        return "Car{" + "make=" + make + ", model=" + model + ", maxSpeedKmph=" + maxSpeedKmph + ", secTo100Kmph=" + secTo100Kmph + ", litersPer100km=" + litersPer100km + '}';
    }
   

    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
    }

    @Override
    public int compareTo(Car o) {   
        //System.out.printf("Using compareTo function between %s and %s \n",model,o.model);
        int comparor = make.compareToIgnoreCase(o.make);
        if(comparor==0){
            return model.compareToIgnoreCase(o.model);
        }else{
            return comparor;
        }
    }        
}


