/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarsMSFT;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author 莫亚泥
 */
public class SqlServerAPI {

    Connection con = null;

    public SqlServerAPI() {
        String connectionUrl = "jdbc:sqlserver://astonl.database.windows.net:1433;database=first;user=AstonL@astonl;password={ZhuJie123$};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30";
        try {
            // Establish the connection.
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
        } // Handle any errors that may have occurred.
        catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Open SQLServer(Azure) Database failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    public ArrayList<car> selectAllFromCars() {
        ArrayList<car> list = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Create and execute an SQL statement that returns some data.
            String SQL = "SELECT * FROM cars";
            stmt = con.prepareStatement(SQL);
            rs = stmt.executeQuery();
            // Iterate through the data in the result set and display it.
            while (rs.next()) {
                String fts = rs.getString(4);
                FuelType ft = CarsMSFT.StringToFuelType(fts);
                list.add(new car(rs.getLong("id"), rs.getString("makeModel"), rs.getBigDecimal("engineSize"), ft));
            }
        } // Handle any errors that may have occurred.
        catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Select from SQLServer table cars failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null,
                            "Close resultSet failure!\n" + ex.getMessage(),
                            "Database error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null,
                            "Close statement failure!\n" + ex.getMessage(),
                            "Database error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        return list;
    }

    public void insertNewCar(car newCar) {
        String sql = "insert into cars (makeModel,engineSize,fuelType) values(?,?,?)";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            stmt.setString(1, newCar.getMakeModel());
            stmt.setBigDecimal(2, newCar.getEngineSize());
            stmt.setString(3, CarsMSFT.FuelTypeToString(newCar.getFuleType()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Insert data into MySQL table failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteCar(List<car> oldCars) {        
        String sql = "delete from cars where id = ?";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            Iterator<car> it = oldCars.iterator();
            while (it.hasNext()) {                
                stmt.setLong(1, it.next().getId());
                stmt.executeUpdate();
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Delete car from SQLServer table cars failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void updateCar(car oldCar) {
        String sql = "update cars set makeModel = ?, engineSize = ?, fuelType = ? where id=?";
        try (PreparedStatement stmt = con.prepareStatement(sql)) {
            stmt.setString(1, oldCar.getMakeModel());
            stmt.setBigDecimal(2, oldCar.getEngineSize());
            stmt.setString(3, CarsMSFT.FuelTypeToString(oldCar.getFuleType()));
            stmt.setLong(4, oldCar.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Update car from SQLServer table cars failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
