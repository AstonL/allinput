/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarsMSFT;

import java.math.BigDecimal;

/**
 *
 * @author 莫亚泥
 */
public class car {

    private long id;
    private String makeModel;
    private BigDecimal engineSize;
    private FuelType fuelType;

    public car(long id, String makeModel, BigDecimal engineSize, FuelType fuelType) {
        setId(id);
        setMakeModel(makeModel);
        setEngineSize(engineSize);
        setFuleType(fuelType);
    }

    @Override
    public String toString() {
        return "id=" + id + ", makeModel=" + makeModel + ", engineSize=" + engineSize.divide(new BigDecimal("10")) + ", fuelType=" + fuelType;
    }

    public FuelType getFuleType() {
        return fuelType;
    }

    public final void setFuleType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public long getId() {
        return id;
    }

    public final void setId(long id) {
        this.id = id;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public final void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public BigDecimal getEngineSize() {
        return engineSize;
    }

    public final void setEngineSize(BigDecimal engineSize) {
        this.engineSize = engineSize;
    }

}
