/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author 莫亚泥
 */
public class EffectiveSorting {
    
    /**
     * @param args the command line arguments
     */
    static ArrayList<Person> list = new ArrayList<>();
    //static Person[] list = new Person[7];
    public static void main(String[] args) {
        // TODO code application logic here
      /*  
        list[0]=new Person("Jerry", 18);
        list[1]=new Person("qwe", 43);
        list[2]=new Person("dd", 44);
        list[3]=new Person("dsf", 95);
        list[4]=new Person("sdf", 18);
        list[5]=new Person("Jsersdry", 38);
        list[6]=new Person("asddg", 30);        
       // Arrays.sort(list);
        */
        
        list.add(new Person("Jerry", 18));
        list.add(new Person("qwe", 43));
        list.add(new Person("dd", 44));
        list.add(new Person("dsf", 95));
        list.add(new Person("sdf", 18));
        list.add(new Person("Jsersdry", 38));
        list.add(new Person("asddg", 30));
        list.sort(Person.personComparator);
        //Collections.sort(list);
        for(Person p:list){
            System.out.printf("Name:%s  Age:%d\n",p.name,p.age);
        }
    }
    
}
class PersonComparatorByName implements Comparator<Person>{
    @Override
    public int compare(Person o1, Person o2) {
        return o1.compareTo(o2); //To change body of generated methods, choose Tools | Templates.
    }
    
}

class Person implements Comparable<Person> {

    String name;
    int age;
    static Comparator<Person> personComparator = new PersonComparatorByName();

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }   

    @Override
    public int compareTo(Person o) {
        return age-o.age; //To change body of generated methods, choose Tools | Templates.        
        //return name.compareToIgnoreCase(o.name);
    }
}
