/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tododb;

import java.sql.Date;
/**
 *
 * @author 1796199
 */
public class todo {
    private long id;
    private String task;
    private Date dueDate;
    private boolean isDone;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public todo(long id, String task, Date dueDate, boolean isDone) {
        this.id = id;
        this.task = task;
        this.dueDate = dueDate;
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        return "id=" + id + ", task=" + task + ", dueDate=" + dueDate + ", isDone=" + isDone + '}';
    }    
}
