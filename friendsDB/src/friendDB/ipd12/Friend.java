/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package friendDB.ipd12;

/**
 *
 * @author 1796199
 */
enum genderEnum{Na,Male,Female};// Improving your coding habit

public class Friend {

    private long id;
    private String name;

    public Friend(long id, String name) {
        setFriendId(id);
        setFriendName(name);
    }

    @Override
    public String toString() {
        return this.id + "." + name;
    }

    public long getId() {
        return id;
    }

    public final void setFriendId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public final void setFriendName(String name) {
        this.name = name;
    }

}
