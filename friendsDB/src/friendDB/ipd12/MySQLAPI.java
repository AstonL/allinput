/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package friendDB.ipd12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class MySQLAPI {

    /*MySQL connection series at school
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "first1";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";
*/

    /*MySQL connection series at home*/
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "first";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "33110532ninipig";
     
    private Connection conn;

    public void commit() throws SQLException {
        this.conn.commit();
    }

    public void rollback() throws SQLException {
        this.conn.rollback();
    }

    public void setAutoCommit(boolean ifCommit) throws SQLException {
        this.conn.setAutoCommit(ifCommit);
    }

    public MySQLAPI() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                    USERNAME, PASSWORD);
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Open MySQL Database failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insertMySQLFriends(String name) {
        String sql = "INSERT INTO friends  VALUES (null, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, name);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Insert data into MySQL table failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public ArrayList<Friend> selectMySQLFriends() {
        String sql = "select * from friends order by id";
        ArrayList<Friend> l = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                long id = result.getLong("id");
                String name = result.getString("name");
                Friend f = new Friend(id, name);
                l.add(f);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Select from MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
        return l;
    }

    public void updateMySQLFriends(long id, String name) {
        String sql = "update friends set name = ? where id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, name);
            stmt.setLong(2, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Update MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteMySQLFriends(long id) {
        String sql = "delete from friends where id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Delete MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
