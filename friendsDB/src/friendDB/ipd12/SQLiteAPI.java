/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package friendDB.ipd12;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author 莫亚泥
 */
public class SQLiteAPI {

    private Connection conn;

    public SQLiteAPI() {
        try {
            // db parameters
            
            String type = "jdbc:sqlite:";
            String dbPath = "D://AllInput/friendsDB/SQLite-friendsDB/frist.db";
            //String dbPath = "D://jie,zhu/projects/AllInput/friendsDB/SQLite-friendsDB/frist.db";
            File f = new File(dbPath);
            //test if .db exist!
            Scanner test = new Scanner(f);            
            
            String url = type + dbPath;
            // create a connection to the database
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");            
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Open SQLite Database failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    public void insertSQLiteFriends(String name) {
        String sql = "INSERT INTO friends  VALUES (null, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, name);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Insert data into MySQL table failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ArrayList<Friend> selectSQLiteFriends() {
        String sql = "select * from friends order by id";
        ArrayList<Friend> l = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                long id = result.getInt("id");
                String name = result.getString("name");
                Friend f = new Friend(id, name);
                l.add(f);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Select from MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
        return l;
    }
    
    public void updateSQLiteFriends(long id, String name) {
        String sql = "update friends set name = ? where id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, name);
            stmt.setLong(2, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Update MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void deleteSQLiteFriends(long id) {
        String sql = "delete from friends where id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Delete MySQL table friends failure!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
