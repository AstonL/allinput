
package geometry;

import java.util.ArrayList;
import java.util.Comparator;


abstract class GeoObj implements Comparable<GeoObj>{
    String color;

    public GeoObj(String color) {
        this.color = color;
    }
    abstract double getSurface();

    @Override
    public int compareTo(GeoObj o){        
        return color.compareTo(o.toString());
    }
    
}
class GeoObjSurfaceComparator implements Comparator<GeoObj>{

    @Override
    public int compare(GeoObj o1, GeoObj o2) {
        if(o1.getSurface()==o2.getSurface()){return 0;}
        else if(o1.getSurface()>o2.getSurface()){return 1;}
        else{return -1;} //To change body of generated methods, choose Tools | Templates.
    }

    
}
class Circle extends GeoObj{
    double radius;
    public Circle(String color,double radius) {
        super(color);
        this.radius = radius;
    }

    @Override
    double getSurface() {
        return radius*radius*Math.PI; //To change body of generated methods, choose Tools | Templates.
    }
    
}

class Rectangle extends GeoObj{
    double width,length;
    public Rectangle(String color,double width,double length) {
        super(color);
        this.width=width;
        this.length=length;
    }

    @Override
    double getSurface() {
        return width*length;
    }    
}
class volume extends Rectangle {
    double height; 
    public volume(String color, double width, double length) {
        super(color, width, length);
    }    
}
class anothervolume extends volume{
    double triangle;
    public anothervolume(String color, double width, double length,double triangle) {
        super(color, width, length);
        this.triangle=triangle;
    }
    @Override
    double getSurface() {
        return width*length;
    } 
}

public class Geometry{

    
    public static void main(String[] args) {
        GeoObj rectObj = new Rectangle("red", 5.5, 6.3);
        System.out.println(rectObj.getSurface());
        
        GeoObj circleObj =  new Circle("black", 44.24);
        System.out.println(circleObj.getSurface());
        
        ArrayList<GeoObj> geoList = new ArrayList<>();
        geoList.add(new Circle("green", 32));
        geoList.add(new Rectangle("red", 33,346));
        geoList.add(new Circle("white", 382));
        geoList.add(new Rectangle("blue", 33,2));
        
    }    
}
