/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author 1796199
 */
public class quiz3 extends javax.swing.JFrame {

    DefaultListModel<trip> tripListModel=new DefaultListModel<>();
    private Connection conn;
    
    public Date convertInputStringDateToSQLDate(String strDate){
        java.util.Date utilDueDate=null;
        Date sqlDueDate=null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        try {
            utilDueDate=formatter.parse(strDate);
            sqlDueDate=new Date(utilDueDate.getTime());            
        } catch (ParseException ex) {
            ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Convert String to SQLDate failure!\n"+ex.getMessage(),
                        "convert error",
                        JOptionPane.ERROR_MESSAGE);
        }
        return sqlDueDate;
    }
    
    public java.util.Date convertInputStringDateToUtilDate(String strDate){
        java.util.Date utilDueDate=null;        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        try {
            utilDueDate=formatter.parse(strDate);                        
        } catch (ParseException ex) {
            ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Convert String to UtilDate failure!\n"+ex.getMessage(),
                        "Convert error",
                        JOptionPane.ERROR_MESSAGE);
        }
        return utilDueDate;
    }
    
    public void loadTripItems(){
        tripListModel.clear();
        String sql = "SELECT * FROM trips";
        Statement statement = null;
        ResultSet result;
        try{
            statement = conn.createStatement();
            result = statement.executeQuery(sql);
            int count = 0;
            while (result.next()) {
                int id = result.getInt(1);
                String name = result.getString(2);  
                Date redDate=result.getDate(3);
                String destination = result.getString(4);                
                String isFirst=result.getString(5);
                Date depDate = result.getDate(6);
                trip tf = new trip(id,name,depDate,redDate,destination,isFirst.equals("true"));
                tripListModel.addElement(tf);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Loading trips data failure!\n"+ex.getMessage(),
                        "Loading error",
                        JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * Creates new form quiz3
     */
    public quiz3() {
        
        String dbURL = "jdbc:mysql://localhost:3306/quiz3";
        String username = "root";
        String password = "root";       
        
        try {

            conn = DriverManager.getConnection(dbURL, username, password);
            if (conn != null) {
                System.out.println("Connected");
            }            
            initComponents();   
            loadTripItems();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                        "Connect to database table quiz3 failure!\n"+ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }           
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        lstTripInfo = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbID = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        tfRedDate = new javax.swing.JTextField();
        tfDest = new javax.swing.JTextField();
        tfDepDate = new javax.swing.JTextField();
        cbFirstClass = new javax.swing.JCheckBox();
        btDel = new javax.swing.JButton();
        btAdd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lstTripInfo.setModel(tripListModel);
        lstTripInfo.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstTripInfo.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                lstTripInfoVetoableChange(evt);
            }
        });
        lstTripInfo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstTripInfoValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstTripInfo);

        jLabel1.setText("ID:");

        jLabel2.setText("Name:");

        jLabel3.setText("DepDate:");

        jLabel4.setText("RedDate:");

        jLabel5.setText("Destination:");

        tfName.setText("1-50 bits");

        tfRedDate.setText("YYYY/MM/DD");

        tfDest.setText("1-50 bits");

        tfDepDate.setText("YYYY/MM/DD");

        cbFirstClass.setText("1st Class");

        btDel.setText("Delete");
        btDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDelActionPerformed(evt);
            }
        });

        btAdd.setText("ADD");
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbFirstClass)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(tfName)
                        .addComponent(tfDepDate, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                        .addComponent(tfRedDate)
                        .addComponent(tfDest)
                        .addComponent(lbID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btDel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lbID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfDepDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfRedDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfDest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbFirstClass)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btDel)
                    .addComponent(btAdd))
                .addGap(12, 12, 12))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
        // TODO add your handling code here:
        
        boolean isDone=cbFirstClass.isSelected();
        String depDate=tfDepDate.getText();
        String redDate=tfRedDate.getText();
        String name=tfName.getText();
        String destination=tfDest.getText();
        Date sqldepDate=convertInputStringDateToSQLDate(depDate);    
        Date sqlredDate=convertInputStringDateToSQLDate(redDate);
        
        //String sql = String.format("INSERT INTO todos (task,dueDate,isDone)  VALUES ('%s','%s','%s')", tfTastDetail.getText(),tfDueDate.getText(),""+isDone);
        String sql="INSERT INTO trips VALUES (null,?,?,?,?,?)";
        PreparedStatement statement;
        try {
            statement = conn.prepareStatement(sql);            
            statement.setString(1, name);
            statement.setDate(2, sqlredDate);
            statement.setString(3, destination);
            statement.setString(4, ""+isDone);
            statement.setDate(5, sqldepDate);
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("A new user was inserted successfully!");
            } else {
                System.out.println("Inserting is failure! ");
            }
            cbFirstClass.setSelected(false);
            tfDepDate.setText("YYYY/MM/DD");
            tfRedDate.setText("YYYY/MM/DD");
            tfName.setText("1 -50 bits");
            tfDest.setText("1 -50 bits");            
            loadTripItems();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Inserting into table trips has problem!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btAddActionPerformed

    private void btDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDelActionPerformed
        // TODO add your handling code here:
        trip tf = lstTripInfo.getSelectedValue();
        //int index = ltFriends.getSelectedIndex();
        if (tf == null) {
            return;
        }

        Object[] options = {"Delete", "Cancel"};
        int decision = JOptionPane.showOptionDialog(this,
                "Are you sure you want to delete?" + tf,
                "Confirm deletion",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null, //do not use a custom Icon
                options, //the titles of buttons
                options[1]); //default button title
        if (decision == JOptionPane.YES_OPTION) {
            //Friend tf = friendsListModel.getElementAt(index);
            String sql = "DELETE FROM trips WHERE id=?";
            PreparedStatement statement;

            try {
                statement = conn.prepareStatement(sql);
                statement.setLong(1, tf.getId());
                int rowsDeleted = statement.executeUpdate();
                if (rowsDeleted > 0) {
                    System.out.println("A user was deleted successfully!");
                }
                loadTripItems();
            } catch (SQLException ex) {
                ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "delete item from todos table has problem!\n" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btDelActionPerformed

    private void lstTripInfoVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_lstTripInfoVetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_lstTripInfoVetoableChange

    private void lstTripInfoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstTripInfoValueChanged
        // TODO add your handling code here:
        trip tf = lstTripInfo.getSelectedValue();
        //int index = ltFriends.getSelectedIndex();
        if (tf != null) {
            lbID.setText(""+tf.getId()); 
            tfName.setText(tf.getName());
            tfDepDate.setText(tf.getDepDate().toString().replace("-", "/"));
            tfRedDate.setText(tf.getRedDate().toString().replace("-", "/"));
            tfDest.setText(tf.getDest());
            cbFirstClass.setSelected(tf.getFirstClass());            
        }  
    }//GEN-LAST:event_lstTripInfoValueChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(quiz3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(quiz3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(quiz3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(quiz3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new quiz3().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btDel;
    private javax.swing.JCheckBox cbFirstClass;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbID;
    private javax.swing.JList<trip> lstTripInfo;
    private javax.swing.JTextField tfDepDate;
    private javax.swing.JTextField tfDest;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfRedDate;
    // End of variables declaration//GEN-END:variables
}
