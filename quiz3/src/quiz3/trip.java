/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3;

import java.sql.Date;

/**
 *
 * @author 1796199
 */
public class trip {
    private long id;
    private String name;
    private Date depDate;
    private Date redDate;
    private  String dest;
    private boolean isFirstClass;
   

    @Override
    public String toString() {
        return  "id=" + id + ",name=" + name + ",depDate=" + depDate + ",redDate=" + redDate +",destination=" + dest + ",isFirstClass=" + isFirstClass+'}'; //To change body of generated methods, choose Tools | Templates.
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDepDate() {
        return depDate;
    }

    public void setDepDate(Date depDate) {
        this.depDate = depDate;
    }

    public Date getRedDate() {
        return redDate;
    }

    public void setRedDate(Date redDate) {
        this.redDate = redDate;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public boolean getFirstClass() {
        return isFirstClass;
    }

    public void setIsFirstClass(boolean isFirstClass) {
        this.isFirstClass = isFirstClass;
    }

    public trip(long id, String name, Date depDate, Date redDate, String dest, boolean isFirstClass) {
        
        this.id = id;
        this.name = name;
        this.depDate = depDate;
        this.redDate = redDate;
        this.dest = dest;
        this.isFirstClass = isFirstClass;
       
    }
    

    
    
}
