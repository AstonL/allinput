/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testtimeserieschart;
import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.constant.TimePeriod;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
/**
 *
 * @author 莫亚泥
 */
public class TestTimeSeriesChart {

    public TestTimeSeriesChart() {
        build();
    }
    private void build() {
		FontBuilder boldFont = stl.fontArialBold().setFontSize(12);

		TextColumnBuilder<Date> orderDateColumn = col.column("Order date", "orderdate", type.dateYearToMonthType());
		TextColumnBuilder<Integer> quantityColumn = col.column("Quantity", "quantity", type.integerType());
		TextColumnBuilder<BigDecimal> priceColumn = col.column("Price", "price", type.bigDecimalType());
		try {
			report()
					.setTemplate(Templates.reportTemplate)
					.columns(orderDateColumn, quantityColumn, priceColumn)
					.title(Templates.createTitleComponent("TimeSeriesChart"))
					.summary(
							cht.timeSeriesChart()
									.setTitle("Time series chart")
									.setTitleFont(boldFont)
									.setTimePeriod(orderDateColumn)
									.setTimePeriodType(TimePeriod.MONTH)
									.series(
											cht.serie(quantityColumn), cht.serie(priceColumn))
									.setTimeAxisFormat(
											cht.axisFormat().setLabel("Date")))
					.pageFooter(Templates.footerComponent)
					.setDataSource(createDataSource())
					.show();
		} catch (DRException e) {
			e.printStackTrace();
		}
	}

	private JRDataSource createDataSource() {
		DRDataSource dataSource = new DRDataSource("orderdate", "quantity", "price");
		dataSource.add(toDate(2010, 1), 50, new BigDecimal(200));
		dataSource.add(toDate(2010, 2), 110, new BigDecimal(450));
		dataSource.add(toDate(2010, 3), 70, new BigDecimal(280));
		dataSource.add(toDate(2010, 4), 250, new BigDecimal(620));
		dataSource.add(toDate(2010, 5), 100, new BigDecimal(400));
		dataSource.add(toDate(2010, 6), 80, new BigDecimal(320));
		dataSource.add(toDate(2010, 7), 180, new BigDecimal(490));
		return dataSource;
	}

	private Date toDate(int year, int month) {
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month - 1);
		return c.getTime();
	}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new TestTimeSeriesChart();
    }
    
}
